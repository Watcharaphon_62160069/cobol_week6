       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. WATCHARAPHON.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
           ORGANIZATION IS LINE SEQUENTIAL
       .
       DATA DIVISION. 
       FILE SECTION.
       FD  EMP-FILE.
       01  EMP-DETAILS.
           88 END-OF-EMP-FILE VALUE HIGH-VALUE .
           05 EMP-SSN  PIC 9(9).
           05 EMP-NAME.
              10 EMP-SURNAME PIC X(15).
              10 EMP-FORNAME PIC X(10).
           05 EMP-DATE-OF-BRITH.
              10 EMP-YOB PIC 9(4).
              10 EMP-MOB PIC 9(2).
              10 EMP-DOB PIC 9(2).
           05 EMP-GENDER PIC X.  

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE 
              MOVE "123456789"              TO EMP-SSN
              MOVE "WATCHARAPHON"           TO EMP-SURNAME 
              MOVE "CHANBUN"                TO EMP-FORNAME
              MOVE "20000612"               TO EMP-DATE-OF-BRITH 
              MOVE "M"                      TO EMP-GENDER
              WRITE EMP-DETAILS

              MOVE "123456780"              TO EMP-SSN
              MOVE "FON"                    TO EMP-SURNAME 
              MOVE "PRACHUAPKHIRIKHAN"      TO EMP-FORNAME
              MOVE "20011212"               TO EMP-DATE-OF-BRITH 
              MOVE "F"                      TO EMP-GENDER
              WRITE EMP-DETAILS
              
              MOVE "103456789"              TO EMP-SSN
              MOVE "NEW"                    TO EMP-SURNAME 
              MOVE "KANCHANABURI"      TO EMP-FORNAME
              MOVE "20011224"               TO EMP-DATE-OF-BRITH 
              MOVE "M"                      TO EMP-GENDER

              WRITE EMP-DETAILS
           CLOSE EMP-FILE.
           GOBACK.
